import { scrollActiv, setRRSSLinks } from '../../common_projects_utilities/js/pure-branded-helpers';
import { isMobile, isElementInViewport } from '../../common_projects_utilities/js/dom-helpers';
//Necesario para importar los estilos de forma automática en la etiqueta 'style' del html final
import '../css/main.scss';

let currentScroll = 0;
let scrollTop;
let viewportWidth = window.innerWidth;
let viewportHeight = window.innerHeight;
let sharing = document.getElementById("sharing");
let sectionHeaders = document.getElementsByClassName('section');
let menuTitles = [
	"01 · Gobernanza, preparación o la toma de decisiones",
	"02 · Fuentes de información",
	"03 · Acciones tomadas y alternativas disponibles",
	"04 · Aspectos sociales",
	"05 · Comunicación de riesgo y recomendaciones a la población"
];
let testimonials = document.getElementsByClassName("testimonial--featured");
let phrases = document.getElementsByClassName("testimonial__phrase");
let logoFooter = document.getElementById("logoFooter");


setRRSSLinks();
if ( viewportWidth > 993 ) {
	configureMenu();
}
configureTestimonialPhrases();


/* LISTENERS */
document.getElementById("arrowHeader").addEventListener("click", function(e){
	e.preventDefault();
	if ( isMobile() ) {
		document.getElementById("intro").scrollIntoView({behavior: "smooth"});
	} else {
		document.getElementById("mainContent").scrollIntoView({behavior: "smooth"});
	}
})

document.getElementById("menuToogle").addEventListener("click", function(e){
	e.preventDefault();
	if ( document.getElementById("menu").classList.contains("menu--toggled") ) {
		hideMenu();
	} else {
		showMenu();
	}
})

let menuLinks = document.getElementsByClassName("menu__link");
for (let i = 0; i < menuLinks.length; i++) {
	menuLinks[i].addEventListener("click", function(e){
		if ( !this.classList.contains("menu__link--external") ) {
			e.preventDefault();
			hideMenu();
			document.getElementById(this.getAttribute("href")).scrollIntoView({behavior: "smooth"});
		}
	});
}

window.addEventListener("scroll", function(){

	scrollActiv();

	scrollTop = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop);
	if ( scrollTop > viewportHeight ){
		sharing.classList.add("visible");
	} else {
		sharing.classList.remove("visible");
	}
	if (isMobile()) {
		if (isElementInViewport(logoFooter)) {
			sharing.classList.remove("visible");
		}
	}

	for (var i = 0; i < sectionHeaders.length; i++) {
		if(sectionHeaders[i].getBoundingClientRect().top > 0 && sectionHeaders[i].getBoundingClientRect().top < 120) {
			if(window.scrollY > currentScroll){
				document.getElementById('menuTitle').textContent = menuTitles[i];
			} else {
				if(i == 0){
					document.getElementById('menuTitle').textContent = menuTitles[0];
				} else {
					document.getElementById('menuTitle').textContent = menuTitles[i-1];
				}				
			}
			currentScroll = window.scrollY;
		}
    }

	for (let i = 0; i < testimonials.length; i++) {
		if(isElementInViewport(testimonials[i])) {
			if(!testimonials[i].classList.contains("testimonial--visible")) {
				testimonials[i].classList.add("testimonial--visible");
			}
		}
	}

	for (let i = 0; i < phrases.length; i++) {
		if(isElementInViewport(phrases[i])) {
			if(!phrases[i].classList.contains("testimonial__phrase--animated")) {
				phrases[i].classList.add("testimonial__phrase--animated");
				let target = phrases[i].id;
				animatePhrase(target);
			}
		}
	}

});


/* MENU */
function configureMenu() {
	let menuItems = document.getElementsByClassName("menu__item");
	for (let i = 0; i < menuItems.length; i++ ) {
		menuItems[i].style.paddingLeft = `${document.getElementById("menuContainer").getBoundingClientRect().left}px`;
	}
}

function showMenu() {
	//Actualizamos icono
	document.getElementById("menuIcon").src = "https://www.ecestaticos.com/file/45a8e920a72504f5470249a7b517ba49/1614941274-icono-cerrar.svg";
	//Mostramos contenido del menú
	document.getElementById("menu").classList.add("menu--toggled");
	//Mostramos overlay sobre el contenido
	document.getElementById("overlay").classList.add("overlay--visible");
	//Deshabilitamos scroll en página
	document.body.style.overflow = "hidden"; 
}

function hideMenu() {
	//Actualizamos icono
	document.getElementById("menuIcon").src = "https://www.ecestaticos.com/file/5cc388f38ab4b808456b2fd0cb5a6e1f/1614936647-icono-menu.svg";
	//Ocultamos contenido del menú
	document.getElementById("menu").classList.remove("menu--toggled");
	//Ocultamos overlay sobre el contenido
	document.getElementById("overlay").classList.remove("overlay--visible");
	//Habilitamos scroll en página
	document.body.style.overflow = "auto"; 
}


/* TESTIMONIALS */
function configureTestimonialPhrases() {
	for (let i = 0; i < phrases.length; i++) {
		phrases[i].innerHTML = phrases[i].textContent.replace(/\S/g, "<span class='letter'>$&</span>");
	}
}

function animatePhrase(phrase) {
	anime.timeline()
		.add({
			targets: `#${phrase} .letter`,
			opacity: [0,1],
			easing: "easeInOutQuad",
			//duration: 2250,
			duration: 1250,
			//delay: (el, i) => 150 * (i+1)
			delay: (el, i) => 50 * (i+1)
		});
}